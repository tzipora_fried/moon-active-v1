const mongoose = require('mongoose');
const config = require('./config')
mongoose.connect(config.DB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});