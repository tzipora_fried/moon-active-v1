
const promotion = require('../models/promotion');

class promotionRepository {

  constructor(model) {
    this.model = model;
  }

  create(newPromotion) {
    const promotion = new this.model({...newPromotion});
    return promotion.save()
  }

  findAll() {
    return this.model.find();
  }

  createMassPromotions(){
    this.model.createCollection
  }

  paginate(skip, take) {
    return this.model.paginate({}, {offset: skip, limit: take})
  }

  findById(id) {
    return this.model.findById(id);
  }

  deleteById(id) {
    return this.model.findByIdAndDelete(id);
  }

  updateById(id, object) {
    const query = { _id: id };
    return this.model.findOneAndUpdate(query, { $set: { name: object.name, done: object.done } });
  }
}

module.exports = new promotionRepository(promotion);